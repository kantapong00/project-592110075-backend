package dv.project.moneygo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MoneygoApplication

fun main(args: Array<String>) {
	runApplication<MoneygoApplication>(*args)
}
