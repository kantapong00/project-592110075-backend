package dv.project.moneygo.config

import dv.project.moneygo.entity.*
import dv.project.moneygo.repository.TransactionRepository
import dv.project.moneygo.repository.UserRepository
import dv.project.moneygo.repository.WalletRepository
import dv.project.moneygo.security.entity.Authority
import dv.project.moneygo.security.entity.AuthorityName
import dv.project.moneygo.security.entity.JwtUser
import dv.project.moneygo.security.repository.AuthorityRepository
import dv.project.moneygo.security.repository.JwtUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.time.LocalDate
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var walletRepository: WalletRepository

    @Autowired
    lateinit var transactionRepository: TransactionRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var jwtUserRepository: JwtUserRepository

    @Transactional
    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val userl = User("bank", "kantapong", "kantapong40202@hotmail.com", "555", "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/12509577_1015363731856842_3365828373799268203_n.jpg?_nc_cat=109&_nc_ht=scontent-kut2-2.xx&oh=07b270db3192329ccab30a0985b3ecf8&oe=5D030E7A")
        val userJwt = JwtUser(
                username = "User",
                password = encoder.encode("password"),
                email = userl.email,
                enabled = true,
                firstname = userl.firstName,
                lastname = userl.lastName
        )
        userRepository.save(userl)
        jwtUserRepository.save(userJwt)
        userl.jwtUser = userJwt
        userJwt.user = userl
        userJwt.authorities.add(auth2)
        userJwt.authorities.add(auth3)


    }


    @Transactional
    override fun run(args: ApplicationArguments?) {

        var user1 = userRepository.save( User("bank@gmail.com","123456789","kantapong","kasemsan","https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/36771308_1903511796353802_5414205579717509120_n.jpg?_nc_cat=105&_nc_ht=scontent-kut2-1.xx&oh=0700fb4b078e080382b85b119644b40c&oe=5D1201DA",UserStatus.ACTIVE))
        var wallet1 = walletRepository.save( Wallet("Bank",24800.00,false))
        var transaction1 = transactionRepository.save( Transaction((LocalDate.parse("2019-03-23")),TransactionType.INCOME,TransactionCategory.AWARDS,4500.00,"WIN ONLINE GAME"))

        user1.wallets.add(wallet1)
        wallet1.transactions.add(transaction1)
        loadUsernameAndPassword()


    }



}
