package dv.project.moneygo.entity

enum class TransactionType {
    INCOME, EXPENSES
}