package dv.project.moneygo.entity

import javax.persistence.*

@Entity
data class User(override var email: String? = null,
                    override var password: String? = null,
                    override var firstName: String? = null,
                    override var lastName: String? = null,
                    override var imageUrl: String? = null,
                    override var userStatus: UserStatus? = UserStatus.PENDING
                ) : UserAccount( firstName,
                                 lastName,
                                 email,
                                 password,
                                 userStatus,
                                 imageUrl )
{
    @OneToMany
    var wallets = mutableListOf<Wallet>()
}
