package dv.project.moneygo.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Transaction(
        var date: LocalDate? = null,
        var transactionType: TransactionType? = null,
        var transactionCategory: TransactionCategory? = null,
        var money: Double? = null,
        var note: String? = null
){
    @Id
    @GeneratedValue
    var id:Long? = null

}
