package dv.project.moneygo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class Wallet(var name: String? = null,
                  var money: Double? = null,
                  var isDeleted: Boolean = false){
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToMany
    var transactions = mutableListOf<Transaction>()
}
