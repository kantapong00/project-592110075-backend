package dv.project.moneygo.entity

import dv.project.moneygo.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class UserAccount(
    open var email: String? = null,
    open var password: String? = null,
    open var firstName: String? = null,
    open var lastName: String? = null,
    open var userStatus: UserStatus? = null,
    open var imageUrl: String? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var jwtUser: JwtUser? = null
}
