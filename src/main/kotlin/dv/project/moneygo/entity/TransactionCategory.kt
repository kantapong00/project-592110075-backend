package dv.project.moneygo.entity

enum class TransactionCategory {
    SALARY,
    TAX,
    AWARDS,
    FOOD,
    HEALTH,
    SHOPPING,
    SALE,
    FAMILY,
    PET,
    GIFT
}