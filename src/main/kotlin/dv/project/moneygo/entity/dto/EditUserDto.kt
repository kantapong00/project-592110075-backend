package dv.project.moneygo.entity.dto

data class EditUserDto ( var firstName: String? = null,
                         var lastName: String? = null,
                         var ImageUrl: String? = null,
                         var password: String? = null
                         )