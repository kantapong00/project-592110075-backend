package dv.project.moneygo.entity.dto

data class UserRegisterDto(
        var email: String? = null,
        var password: String? = null,
        var firstName: String? = null,
        var lastName: String? = null
)