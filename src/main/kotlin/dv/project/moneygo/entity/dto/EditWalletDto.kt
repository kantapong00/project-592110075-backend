package dv.project.moneygo.entity.dto

data class EditWalletDto( var name: String? = null,
                          var id: Long? = null)