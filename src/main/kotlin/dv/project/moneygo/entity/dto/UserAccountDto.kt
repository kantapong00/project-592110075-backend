package dv.project.moneygo.entity.dto

data class UserAccountDto(
        var email: String? = null,
        var firstName: String? = null,
        var lastName: String? = null
        )
