package dv.project.moneygo.entity.dto

import dv.project.moneygo.entity.TransactionCategory
import dv.project.moneygo.entity.TransactionType
import java.time.LocalDate

data class TransactionDto( var id: Long? = null,
                           var date: LocalDate? = null,
                           var transactionType: TransactionType? = null,
                           var transactionCategory: TransactionCategory? = null,
                           var money: Double? = null,
                           var note: String? = null )