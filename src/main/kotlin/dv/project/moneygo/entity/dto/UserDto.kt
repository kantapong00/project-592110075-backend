package dv.project.moneygo.entity.dto

data class UserDto( var email: String? = null,
                    var password: String? = null,
                    var firstName: String? = null,
                    var lastName: String? = null,
                    var imageUrl: String? = null,
                    var id: Long? = null,
                    var wallets: List<WalletDto>? = emptyList())