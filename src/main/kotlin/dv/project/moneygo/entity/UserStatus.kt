package dv.project.moneygo.entity

enum class UserStatus{
    ACTIVE,DELETED,PENDING
}
