package dv.project.moneygo.repository


import dv.project.moneygo.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<User, Long>{

}