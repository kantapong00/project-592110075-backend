package dv.project.moneygo.repository

import dv.project.moneygo.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface WalletRepository: CrudRepository<Wallet, Long> {
    fun findByIsDeletedIsFalse(): List<Wallet>
}
