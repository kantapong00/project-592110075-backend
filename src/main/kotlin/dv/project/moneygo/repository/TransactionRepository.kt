package dv.project.moneygo.repository

import dv.project.moneygo.entity.Transaction
import org.springframework.data.repository.CrudRepository

interface TransactionRepository: CrudRepository<Transaction, Long>{

}