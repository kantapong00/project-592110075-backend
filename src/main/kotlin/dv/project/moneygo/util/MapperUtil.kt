package dv.project.moneygo.util

import dv.project.moneygo.entity.Transaction
import dv.project.moneygo.entity.User
import dv.project.moneygo.entity.Wallet
import dv.project.moneygo.entity.dto.*
import dv.project.moneygo.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapUserDto(user: User): UserDto
    fun mapUserDto(user: List<User>): List<UserDto>
    @InheritInverseConfiguration
    fun mapUserDto(userDto: UserDto): User

    fun mapWalletDto(wallet: Wallet?): WalletDto?
    fun mapWalletDto(wallet: List<Wallet>): List<WalletDto>
    @InheritInverseConfiguration
    fun mapWalletDto(walletDto: WalletDto): Wallet

    fun mapEditWalletDto(wallet: Wallet): EditWalletDto

    fun mapEditUserDto(user: User): EditUserDto

    fun mapTransactionDto(transaction: Transaction?): TransactionDto?
    fun mapTransactionDto(transaction: List<Transaction>): List<Transaction>
    @InheritInverseConfiguration
    fun mapTransactionDto(transactionDto: TransactionDto): Transaction

    fun mapUserAccountDto(user: User): UserAccountDto

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>

    fun mapUserRegisterDto(user: User):UserRegisterDto
    @InheritInverseConfiguration
    fun mapUserRegisterDto(userRegisterDto: UserRegisterDto): User


}
