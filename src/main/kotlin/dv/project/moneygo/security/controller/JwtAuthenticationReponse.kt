package dv.project.moneygo.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)