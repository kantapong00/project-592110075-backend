package dv.project.moneygo.security.repository

import dv.project.moneygo.security.entity.Authority
import dv.project.moneygo.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}