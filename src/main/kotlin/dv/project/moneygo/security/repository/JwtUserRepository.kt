package dv.project.moneygo.security.repository

import dv.project.moneygo.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface JwtUserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}