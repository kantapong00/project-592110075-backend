package dv.project.moneygo.dao

import dv.project.moneygo.entity.Transaction


interface TransactionDao{
    fun getAllTransactions(): List<Transaction>
    fun getTransactionById(id: Long): Transaction?
    fun save(transaction: Transaction): Transaction


}