package dv.project.moneygo.dao

import dv.project.moneygo.entity.Wallet
import dv.project.moneygo.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class WalletDaoBDImpl : WalletDao {
    override fun findById(id: Long?): Wallet {
        return walletRepository.findById(id!!).orElse(null)
    }

    override fun getWalletById(id: Long): Wallet? {
        return walletRepository.findById(id).orElse(null)
    }

    override fun save(wallet: Wallet): Wallet {
        return walletRepository.save(wallet)
    }

    override fun getAllWallets(): List<Wallet> {
//        return walletRepository.findAll().filterIsInstance(Wallet::class.java)
        return walletRepository.findByIsDeletedIsFalse()
    }

    @Autowired
    lateinit var walletRepository: WalletRepository

}
