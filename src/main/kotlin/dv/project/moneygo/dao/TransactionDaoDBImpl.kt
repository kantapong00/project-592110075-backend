package dv.project.moneygo.dao

import dv.project.moneygo.entity.Transaction
import dv.project.moneygo.repository.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class TransactionDaoDBImpl : TransactionDao{

    override fun save(transaction: Transaction): Transaction {
        return transactionRepository.save(transaction)
    }

    override fun getTransactionById(id: Long): Transaction? {
        return transactionRepository.findById(id).orElse(null)
    }

    override fun getAllTransactions(): List<Transaction> {
        return transactionRepository.findAll().filterIsInstance(Transaction::class.java)
    }

    @Autowired
    lateinit var transactionRepository: TransactionRepository
}