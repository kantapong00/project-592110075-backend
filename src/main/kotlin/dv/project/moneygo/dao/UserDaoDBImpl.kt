package dv.project.moneygo.dao

import dv.project.moneygo.entity.User
import dv.project.moneygo.repository.UserRepository
import dv.project.moneygo.security.entity.Authority
import dv.project.moneygo.security.entity.AuthorityName
import dv.project.moneygo.security.entity.JwtUser
import dv.project.moneygo.security.repository.AuthorityRepository
import dv.project.moneygo.security.repository.JwtUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoDBImpl : UserDao{
    override fun saveRegister(userRegister: User): User {
        val roleUser = Authority(name = AuthorityName.ROLE_CUSTOMER)
        authorityRepository.save(roleUser)
        val encoder = BCryptPasswordEncoder()
        val userl = userRegister
        val userJwt = JwtUser(
                username = userl.email,
                password = encoder.encode(userl.password),
                email = userl.email,
                enabled = true,
                firstname = userl.firstName,
                lastname = userl.lastName
        )
        userRepository.save(userl)
        jwtUserRepository.save(userJwt)
        userl.jwtUser = userJwt
        userJwt.user = userl
        userJwt.authorities.add(roleUser)
        return userRepository.save(userRegister)

    }

    override fun findById(id: Long): User {
        return userRepository.findById(id).orElse(null)
    }

    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun getUserById(id: Long): User? {
        return userRepository.findById(id).orElse(null)
    }

    override fun getAllUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository

    @Autowired
    lateinit var jwtUserRepository: JwtUserRepository
}