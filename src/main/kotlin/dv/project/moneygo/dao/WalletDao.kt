package dv.project.moneygo.dao

import dv.project.moneygo.entity.Wallet

interface WalletDao {
    fun getAllWallets(): List<Wallet>
    fun save(wallet: Wallet): Wallet
    fun getWalletById(id: Long): Wallet?
    fun findById(id: Long?): Wallet
}