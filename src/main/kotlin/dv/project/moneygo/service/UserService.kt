package dv.project.moneygo.service

import dv.project.moneygo.entity.User
import dv.project.moneygo.entity.dto.EditUserDto
import dv.project.moneygo.entity.dto.UserRegisterDto

interface UserService {
    fun getAllUsers(): List<User>
    fun save(user: User): User
    fun getUserByUserId(userId: Long): User?
    fun saveEditUser(userId: Long, editUserDto: EditUserDto): User
    fun saveRegister(userRegisterDto: User): User
}