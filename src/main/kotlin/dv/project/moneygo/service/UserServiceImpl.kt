package dv.project.moneygo.service

import dv.project.moneygo.dao.UserDao
import dv.project.moneygo.entity.User
import dv.project.moneygo.entity.dto.EditUserDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class UserServiceImpl : UserService{
    override fun saveRegister(userRegisterDto: User): User {
        return userDao.saveRegister(userRegisterDto)
    }


    override fun saveEditUser(userId: Long, editUserDto: EditUserDto): User {
        val user = userDao.findById(userId)
        user.firstName = editUserDto.firstName
        user.lastName = editUserDto.lastName
        user.password = editUserDto.password
        user.imageUrl = editUserDto.ImageUrl
        return userDao.save(user)
    }

    override fun getUserByUserId(userId: Long): User? {
        return userDao.getUserById(userId)
    }

    @Transactional
    override fun save(user: User): User {
        return userDao.save(user)
    }

    override fun getAllUsers(): List<User> {
        return userDao.getAllUsers()
    }

    @Autowired
    lateinit var userDao: UserDao

}