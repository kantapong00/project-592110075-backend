package dv.project.moneygo.service

import dv.project.moneygo.entity.Wallet
import dv.project.moneygo.entity.dto.EditWalletDto

interface WalletService{
    fun getAllWallets(): List<Wallet>
    fun save(userId: Long, wallet: Wallet): Wallet
    fun remove(id: Long): Wallet?
    fun saveWallet(walletId: Long, editWalletDto: EditWalletDto): Wallet
    fun getWalletById(walletId: Long): Wallet?
}
