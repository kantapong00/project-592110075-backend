package dv.project.moneygo.service

import dv.project.moneygo.entity.Transaction

interface TransactionService {
    fun getAllTransactions(): List<Transaction>
    fun save(walletId: Long, transaction: Transaction): Transaction
}