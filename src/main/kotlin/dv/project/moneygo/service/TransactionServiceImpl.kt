package dv.project.moneygo.service

import dv.project.moneygo.dao.TransactionDao
import dv.project.moneygo.dao.WalletDao
import dv.project.moneygo.entity.Transaction
import dv.project.moneygo.entity.TransactionType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.RuntimeException
import javax.transaction.Transactional

@Service
class TransactionServiceImpl : TransactionService {

    @Transactional
    override fun save(walletId: Long, transaction: Transaction): Transaction {
        val wallet = walletDao.findById(walletId)
        val transaction = transactionDao.save(transaction)
        wallet?.transactions?.add(transaction)
        if(transaction.transactionType == TransactionType.INCOME){
            wallet.money = wallet.money!!.plus(transaction.money!!)
        } else {
            wallet.money = wallet.money!!.minus(transaction.money!!)
        }
        return transaction
    }

    @Transactional
    override fun getAllTransactions(): List<Transaction> {
        return transactionDao.getAllTransactions()
    }

    @Autowired
    lateinit var transactionDao: TransactionDao

    @Autowired
    lateinit var walletDao: WalletDao

}