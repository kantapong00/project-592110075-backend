package dv.project.moneygo.service

import dv.project.moneygo.dao.UserDao
import dv.project.moneygo.dao.WalletDao
import dv.project.moneygo.entity.Wallet
import dv.project.moneygo.entity.dto.EditWalletDto
import dv.project.moneygo.entity.dto.WalletDto
import dv.project.moneygo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.RuntimeException
import javax.transaction.Transactional

@Service
class WalletServiceImpl : WalletService{
    override fun getWalletById(walletId: Long): Wallet? {
        return walletDao.getWalletById(walletId)
    }

    override fun saveWallet(walletId: Long, editWalletDto: EditWalletDto): Wallet {
        val wallet = walletDao.findById(walletId)
        wallet.name = editWalletDto.name
        return walletDao.save(wallet)
    }


    @Transactional
    override fun remove(id: Long): Wallet? {
        val wallet = walletDao.getWalletById(id)
        wallet?.isDeleted = true
        return wallet
    }

    @Transactional
    override fun save(userId: Long, wallet: Wallet): Wallet {
        val user = userDao.getUserById(userId)
        if (user != null) {
            val newWallet = walletDao.save(wallet)
            user?.wallets?.add(newWallet)
            return newWallet
        } else {
            throw RuntimeException("Not found id")
        }
    }
    @Transactional
    override fun getAllWallets(): List<Wallet> {
        return walletDao.getAllWallets()
    }

    @Autowired
    lateinit var userDao: UserDao

    @Autowired
    lateinit var walletDao: WalletDao
}
