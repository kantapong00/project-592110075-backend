package dv.project.moneygo.controller

import dv.project.moneygo.entity.User
import dv.project.moneygo.entity.dto.EditUserDto
import dv.project.moneygo.entity.dto.UserDto
import dv.project.moneygo.service.UserService
import dv.project.moneygo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserController {
    @Autowired
    lateinit var userService: UserService

    @GetMapping("/users")
    fun getAllTransactions(): ResponseEntity<Any>{
        val output = userService.getAllUsers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUserDto(output))
    }

    @PostMapping("/users/add/")
    fun addTransaction( @RequestBody user: UserDto ): ResponseEntity<Any> {
        val output = userService.save( MapperUtil.INSTANCE.mapUserDto(user))
        val outputDto = MapperUtil.INSTANCE.mapUserDto(output)
        outputDto?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    }

    @GetMapping("/users/userId/{id}")
    fun getUserByUserId(@PathVariable("id") userId: Long) : ResponseEntity<Any>{
        val output = userService.getUserByUserId(userId)
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found userId")
    }

    @PutMapping("/users/edit/{userId}")
    fun editUser( @PathVariable("userId") userId: Long,
                    @RequestBody editUserDto: EditUserDto): ResponseEntity<Any> {
        try {
            val output = userService.saveEditUser(userId,editUserDto)
            return ResponseEntity.ok(output)
        } catch ( e: RuntimeException ){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found wallet")
        }

    }

    @PostMapping("/users/register")
    fun register(@RequestBody user: User): ResponseEntity<Any>{
        val output = userService.saveRegister(user)
        val outputDto = MapperUtil.INSTANCE.mapUserRegisterDto(output)
        return ResponseEntity.ok(outputDto)
    }


}