package dv.project.moneygo.controller

import dv.project.moneygo.entity.dto.TransactionDto
import dv.project.moneygo.service.TransactionService
import dv.project.moneygo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.RuntimeException

@RestController
class TransactionController {

    @Autowired
    lateinit var transactionService: TransactionService

    @GetMapping("/transactions")
    fun getAllTransactions(): ResponseEntity<Any> {
        val output = transactionService.getAllTransactions()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapTransactionDto(output))
    }

    @PostMapping("/transactions/add/{walletId}")
    fun addTransaction( @PathVariable("walletId") walletId: Long,
                        @RequestBody transaction: TransactionDto): ResponseEntity<Any> {
        try {
            val output = transactionService.save(walletId, MapperUtil.INSTANCE.mapTransactionDto(transaction))
            val outputDto = MapperUtil.INSTANCE.mapTransactionDto(output)
            outputDto?.let { return ResponseEntity.ok(it) }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
        } catch (e: RuntimeException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found id")
        }
    }

//    @GetMapping("/transaction/type/{id}")
//    fun getUserByUserId(@PathVariable("id") userId: Long) : ResponseEntity<Any>{
//        val output = userService.getUserByUserId(userId)
//        output?.let { return ResponseEntity.ok(output) }
//        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found userId")
//    }
}