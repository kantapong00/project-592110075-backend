package dv.project.moneygo.controller

import dv.project.moneygo.entity.dto.EditWalletDto
import dv.project.moneygo.entity.dto.WalletDto
import dv.project.moneygo.service.WalletService
import dv.project.moneygo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.RuntimeException

@RestController
class WalletController {
    @Autowired
    lateinit var walletService: WalletService

    @GetMapping("/wallets")
    fun getAllTransactions(): ResponseEntity<Any> {
        val output = walletService.getAllWallets()
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapWalletDto(output))
    }

    @PostMapping("/wallets/add/{userId}")
    fun addWallet( @PathVariable("userId") userId: Long,
                        @RequestBody wallet: WalletDto): ResponseEntity<Any> {
        try {
            val output = walletService.save(userId, MapperUtil.INSTANCE.mapWalletDto(wallet))
            val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
            outputDto?.let { return ResponseEntity.ok(it) }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
        } catch (e: RuntimeException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found id")
        }
    }

    @DeleteMapping("/wallets/{walletId}")
    fun deleteWallet(@PathVariable("walletId") id:Long):ResponseEntity<Any>{
        val wallet = walletService.remove(id)
        val outputWallet = MapperUtil.INSTANCE.mapWalletDto(wallet)
        outputWallet?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the wallet id is not found")
    }

    @PutMapping("/wallets/edit/{walletId}")
    fun editWallet( @PathVariable("walletId") walletId: Long,
                    @RequestBody editWalletDto: EditWalletDto): ResponseEntity<Any> {
        try {
            val output = walletService.saveWallet(walletId,editWalletDto)
            return ResponseEntity.ok(output)
        } catch ( e: RuntimeException ){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found wallet")
        }

    }

    @GetMapping("/wallets/walletId/{walletId}")
    fun getWalletById(@PathVariable("walletId") walletId: Long) : ResponseEntity<Any>{
        val output = walletService.getWalletById(walletId)
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found userId")
    }
}
